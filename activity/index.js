console.log("Pokemon game!")


let pokemonTrainerDetails = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["pikachu", " venusaur", "charmander ", "charmeleon"],
    friends: { friend1: 'Joey',
                friend2: 'Misha',
                friend3: 'Mimi'
     },
     talk: function () {
        console.log('Pikachu! I choose you! ')
     }

}
console.log(pokemonTrainerDetails)
console.log("Result of dot notation: ")
console.log(pokemonTrainerDetails.name)
console.log("Result of bracket notation: ")
console.log(pokemonTrainerDetails.pokemon)

console.log("Result of talk method: ")
pokemonTrainerDetails.talk()




function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };
    this.faint = function(){
        console.log(this.name + ' has fainted.');
    }

}



let pikachu = new Pokemon("Pikachu", 3);

console.log(pikachu);

let venusaur = new Pokemon('venusaur', 8);

console.log(venusaur);

let squirtle = new Pokemon('squirtle', 5);

console.log(squirtle);



pikachu.tackle(squirtle);

squirtle.tackle(pikachu);

venusaur.tackle(squirtle)

squirtle.faint(venusaur)