//SECRTION: objects

/*
 	-an object is a data type that is used to represent real world objects.
 	-Info stored in objects are represented in a "key:value" pair
 	-A key is also refered to as a "property" of an object
 	-diff data types may be stored in an object's property creating complex data structures.
	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB

		}
*/

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999,
	price: 100
};

console.log('Result from creating objects');
console.log(cellphone);
console.log(typeof cellphone);


//Creating objects using a constructor function

/*

	-creating a reusable function to create several objects that have the same date structure
	-this is useful for creating multiple instances of instances/copies of an object.


	syntax:
		function objectName (keyA, keyB) {
	
			this.keyA = keyA;
			this.keB =keyB;
		}

*/
//'this.' keyword allows to assign a new object's props by associating them with value recieved from constructor function's parameters. This is a constructor

function Laptop(name, manufactureDate) {

	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop ("Lenov", 2008); //the 'new' operator creates an insatance of an object

console.log("result from creating object using object constructors:");
console.log(laptop);


let myLaptop = new Laptop ("MacBook Air", 2020); //the 'new' operator creates an insatance of an object

console.log("result from creating object using object constructors:");
console.log(myLaptop);

/*
Sir CJ notes:


function Laptop(name, manufactureDate, price){
	this.name = name;
	this.manufactureDate = manufactureDate;
	this.price = price

	// return {name: name, manufactureDate: manufactureDate, price: price}
}

// the 'new' operator creates an instance of an object.

let laptop = new Laptop('Lenovo', 2008, 20000);
console.log('Result from creating objects using object constructors:');
console.log(laptop);

let myLaptop = new Laptop('MacBook Air', 2020, 30000);
console.log('Result from creating objects

*/

// SECTION: Accesing object props

//Using the dot notation

console.log("result from dot notation: " + myLaptop.name);
//review: in array we access it by arrayName[1]


//Using the square bracket notation
console.log("result from square bracket notation: " + myLaptop["name"]);
//this is not reccommended to use though


//Accessing array objects
/*
	-Accessing object props using square bracket notation and indexes can cause confusion.
	-by using the notation, this easily helps us differentiate accesing elements from arrays and props from objects
	-Syntax:
		if array: 
		 arrayName/variable[].name


*/
let array = [laptop, myLaptop]

//console.log(array[0]["manufactureDate"]); ---we can also use this but bad practice 

//array number call the array index and the element name after it is the element inside
console.log(array[1].manufactureDate); 
console.log(array[1].name); 
console.log(array[0].manufactureDate); 

//this is an example of array over objects

let sample = ["Hello", {firstName: "CJ", lastName: "Macaraeg"}, 78];

console.log(sample[1]); //it is index 1 bcoz index 0 is the word hello and index 2 is 78
console.log(sample[2]);

console.log(sample[1].firstName) 
console.log(sample[1].lastName)




// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};

// Initializing/adding object properties using dot notation


car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation:');
console.log(car);

// Initializing/adding object properties using bracket notation
/*
    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
    - This also makes names of object properties to not follow commonly used naming conventions for them
*/

//Addding object props


//car.manufacture_date = 2019; // -- this is a good practice

car['manufacture date'] = 2019;


console.log("Result from adding props using bracket notation: ")
console.log(car)


//Deleting object props
delete car ['manufacture date'];
console.log("Result deleting properties")
console.log(car);


//re-assignong object properties
car.name = "Dodge Charger R/T";
console.log("Result from reassigning props");
console.log(car);




// [Section] Object Methods
/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

// Note: If you access an object method without (), it will return the function definition:

// function() { return this.firstName + " " + this.lastName; }

let person = {
    name: 'John',
    talk: function (){ //talk is the object method
        console.log('Hello my name is ' + this.name); //.this is pangkuha or to get the value of name
    }
}

console.log(person);
console.log('Result from object methods:');
person.talk();


// Adding methods to objects.

person.walk = function() { 
    console.log(this.name + ' walked 25 steps forward.');
};
person.walk();

console.log(person)



// Methods are useful for creating reusable functions that perform tasks related to objects


let friend = {
    firstName: 'Joe',
    lastName: 'Smith',
    address: {
        city: 'Austin',
        country: 'Texas'
    },
    emails: ['joe@mail.com', 'joesmith@email.xyz'],
    introduce: function() {
        console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
    }
};

friend.introduce();


console.log(friend.address) // to access the addresses inside
console.log(friend.address.city) // to access the city inside the order


console.log(friend.emails) // to access the emails
console.log(friend.emails[0]) // to access the specific email
console.log(friend.emails[0])

// you can also try this friend.emails.splice(1,2) 
// or console.log(friend.emails[0], friend.emails[1]) 

/*UMIRAL SIGURO YUNG KADEMONYOHAN NG AKING KAMAY, GUMALAW 
-Sir CJ*/


// SECTION  Real World Application Objects

/*
	-scenario
	1. we would like to create a game that would have pokemon interact with each other
	2. every pokemon would have same set of stats and props and fuction.
*/


// Creating an object constructor instead will help with this process

console.log("Pokemon game!")


function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };
    this.faint = function(){
        console.log(this.name + 'fainted.');
    }

}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 3);
let rattata = new Pokemon('Rattata', 8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects

pikachu.tackle(rattata);
